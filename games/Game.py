import os
from random import randint

from games.Level import Level
from games.Oyuncu import Oyuncu
from games.NPC import Kotu
from games.Actor import Actor


class Game:
    basladimi = False
    karakter = None
    level = None

    def __init__(self):
        print('Oyuna Başlamak için isim girin')
        karakter_ismi = input('isminiz :')
        self.karakter = Oyuncu()
        self.karakter.isim = karakter_ismi
        self.karakter.deneyim_seviyesi = 2
        self.level = Level(self.karakter)
        print('Hoş geldin {} '.format(self.karakter.isim))
        self.run()

    def hasar_hesap(self, kimden, kime):
        hasar = kimden.silahi[0].get('hasar') * kimden.deneyim_seviyesi
        hasardan_kacma = randint(0, 10)
        if hasardan_kacma > 6:
            return 0
        kritik = randint(0, 10)
        if kritik < 3:
            hasar = hasar * 3
        hasar = hasar - (hasar * (0.1 * kime.deneyim_seviyesi))
        return hasar

    def dusecek_esya_ve_altin_hesaplama(self, kotu):
        dusecek_nesne = randint(1, 3)
        esya_dusme_sansi = kotu.get("kim").deneyim_seviyesi * 20
        esya_dusme_yuzdesi = randint(1, 100)
        print("{} altin düştü.".format(kotu.get("kim").altin))
        self.karakter.altin += kotu.get('kim').altin
        if esya_dusme_yuzdesi <= esya_dusme_sansi:
            print('{} level kotuyu oldurdun {} esyasi dustu.'.format(kotu.get(
                "kim").deneyim_seviyesi, Kotu.dusecekler[dusecek_nesne - 1].get("isim")))
            self.karakter.cantasi.append(Kotu.dusecekler[dusecek_nesne - 1])
        else:
            print("Herhangi bir esya düsmedi.")

    def run(self):
        actor = Actor()
        while True:
            os.system('clear')
            print('1 - Haraket et')
            print('2 - Etrafa Bak')
            print('3 - Çantaya Bak')
            print('4 - Kötüleri Ara')
            islem = input('Seçim : ')
            if islem == "1":
                os.system('clear')
                print('1 - Yukarı')
                print('2 - Aşağı')
                print('3 - Sağa')
                print('4 - Sola')
                yon = input('Hangi Yöne :')
                """
                yon = 0 yukarı
                yon = 1 sol
                yon = 2 sag
                yon = 3 asagi
                """
                if yon == '1':
                    self.level.ilerle(1, 0)
                if yon == '4':
                    self.level.ilerle(1, 1)
                if yon == '3':
                    self.level.ilerle(1, 2)
                if yon == '2':
                    self.level.ilerle(1, 3)
                print('ilerledin')
                print(self.level.ana_karakter_kare)
                input('Menüye dönmek için bir tuşa bas')
            if islem == "2":
                self.level.etrafa_bak()
                print('Birisi ile konuşmak istiyor musun ?')
                evet = input('E/H :')
                if evet.strip().lower() == 'e':
                    kim = input('Kimle ?')
                    self.level.etkilesim(int(kim))
                    print('0 - Çıkış')
                    secim = input('Hangisini Satın Alacaksın :')
                    if secim == '0':
                        continue
                    urun = self.level.etkilesim(int(kim), int(secim))
                    if self.karakter.altin >= urun.get('fiyat'):
                        self.karakter.altin -= urun.get('fiyat')
                        self.karakter.cantasi.append(urun)
                        print('kalan altın {}, alışveriş için teşekkürler'.format(
                            self.karakter.altin
                        ))
                    else:
                        print('Yeterli Paran yok')

                    print(self.karakter.cantasi)
            elif islem == "4":
                self.level.kotuleri_ara()
                print('0 - Vazgeç')
                kotu = input('Saldırmak İstediğini Seç :')
                if kotu == "0":
                    continue
                elif not kotu.isdigit():
                    print("Seçiminiz yanlış.")
                kotu_karakter = self.level.kotuler[int(kotu) - 1]
                print(kotu_karakter)
                while True:
                    input('Saldır :')
                    os.system('clear')
                    hasarben = self.hasar_hesap(self.karakter, kotu_karakter.get('kim'))
                    kotu_karakter.get('kim').hasar(hasarben)
                    if kotu_karakter.get('kim').yasiyormu:
                        print('{} hasar vurdun {} can kaldı'.format(hasarben, kotu_karakter.get('kim').can))
                    else:
                        self.dusecek_esya_ve_altin_hesaplama(kotu_karakter)
                        self.karakter.susuzluk -= 10
                        if self.karakter.susuzluk < 0:
                            self.karakter.can -= 4
                            if self.karakter.can <= 0:
                                Actor.yasiyormu = False
                                print("Öldün")
                                exit()
                            self.karakter.susuzluk = 0
                            print(
                                "Kalan susuzluk 0. Susuzlugunu gidermegigin surece canın azalacak.\n"
                                "Kalan canin = {}.".format(self.karakter.can))
                        else:
                            print("Kalan susuzluk {}.".format(self.karakter.susuzluk))
                        self.karakter.aclik -= 10
                        if self.karakter.aclik < 0:
                            self.karakter.can -= 4
                            if self.karakter.can <= 0:
                                Actor.yasiyormu = False
                                print("Öldün")
                                exit()
                            self.karakter.aclik = 0
                            print("Kalan aclik = 0. Acligini gidermedigin surece canın azalacak.\n"
                                  "Kalan canin = {}".format(self.karakter.can))
                        else:
                            print("Kalan aclik {}.".format(self.karakter.aclik))
                        input('Devam etmek için bir tuşa basın')
                        break
                    hasarkotu = self.hasar_hesap(kotu_karakter.get('kim'), self.karakter)
                    self.karakter.hasar(hasarkotu)
                    if self.karakter.yasiyormu:
                        print('{} hasar yedin, canın {} kaldı'.format(hasarkotu, self.karakter.can))
                    else:
                        print('Öldün')
                        exit()

            elif islem == "3":
                item_id = 0
                print('Altın {} / Sağlık {} / Enerji {} / Açlık {} / Susuzluk {}'.format(self.karakter.altin,
                                                                                         self.karakter.can,
                                                                                         self.karakter.enerji,
                                                                                         self.karakter.aclik,
                                                                                         self.karakter.susuzluk))
                print('0 - Çıkış')
                for i in self.karakter.cantasi:
                    item_id += 1
                    print('{} - {}'.format(
                        item_id, i.get('isim')
                    ))
                islem = int(input('Kullanmak İstediğin :'))  # cantanin son elemaninin bastiriyor iden dolayi !!!!!!!!!
                if self.karakter.cantasi[islem - 1].get("tip") == 1:
                    while True:
                        if self.karakter.can == 100:
                            emin_misin=int(input("Canın 100 yine de kullanmak istedigine emin misin?\n1.Evet\n2.Hayır"))
                            if emin_misin == 1:
                                print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                                print(
                                    "{} can doldurdun.\nYeni canın {}.".format(
                                        actor.iyilestirme(self.karakter.cantasi[islem - 1].get("deger"), self.karakter.can),
                                        actor.can))
                                self.karakter.cantasi.pop(islem - 1)
                                break
                            elif emin_misin == 2:
                                break
                            else:
                                print("Yanlış tercih yaptiniz.")
                        else:
                            print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                            print(
                                "{} can doldurdun.\nYeni canın {}.".format(
                                    actor.iyilestirme(self.karakter.cantasi[islem - 1].get("deger"), self.karakter.can),
                                    actor.can))
                            self.karakter.cantasi.pop(islem - 1)
                            break
                elif self.karakter.cantasi[islem - 1].get("tip") == 2:
                    while True:
                        if self.karakter.enerji == 100:
                            emin_misin = int(input("Enerjin 100 yine de kullanmak istedigine emin misin?\n1.Evet\n2.Hayır"))
                            if emin_misin == 1:
                                print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                                print("{} enerji doldurdun.\nYeni enerjin {}.".format(
                                    actor.enerji_doldur(self.karakter.cantasi[islem - 1].get("deger"), self.karakter.enerji),
                                    actor.enerji))
                                self.karakter.cantasi.pop(islem - 1)
                                break
                            elif emin_misin == 2:
                                break
                            else:
                                print("Yanlış tercih yaptiniz.")
                        else:
                            print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                            print("{} enerji doldurdun.\nYeni enerjin {}.".format(
                                actor.enerji_doldur(self.karakter.cantasi[islem - 1].get("deger"), self.karakter.enerji),
                                actor.enerji))
                            self.karakter.cantasi.pop(islem - 1)
                            break
                elif self.karakter.cantasi[islem - 1].get("tip") == 4:
                    while True:
                        if self.karakter.aclik == 100:
                            emin_misin=int(input("Açlığın 100 yine de kullanmak istedigine emin misin?\n1.Evet\n2.Hayır"))
                            if emin_misin == 1:
                                print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                                print(
                                    "{} aclik giderildi.\nYeni acligin {}".format(
                                        actor.aclik_giderme(self.karakter.cantasi[islem - 1].get("deger"), self.karakter.aclik),
                                        actor.aclik
                                    ))
                                self.karakter.cantasi.pop(islem - 1)
                                break
                            elif emin_misin == 2:
                                break
                            else:
                                print("Yanlış tercih yaptınız")
                        else:
                            print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                            print(
                                "{} aclik giderildi.\nYeni acligin {}".format(
                                    actor.aclik_giderme(self.karakter.cantasi[islem - 1].get("deger"),
                                                        self.karakter.aclik),
                                    actor.aclik
                                ))
                            self.karakter.cantasi.pop(islem - 1)
                            break
                elif self.karakter.cantasi[islem - 1].get("tip") == 3:
                    while True:
                        if self.karakter.susuzluk == 100:
                            emin_misin = int(input("Susuzluğun 100 yine de kullanmak istedigine emin misin?\n1.Evet\n2.Hayır"))
                            if emin_misin == 1:
                                print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                                print("{} susuzluk giderildi.\nYeni susuzlugun {}.".format(
                                    actor.susuzluk_giderme(self.karakter.cantasi[islem - 1].get("deger"), self.karakter.susuzluk),
                                    actor.susuzluk))
                                self.karakter.cantasi.pop(islem - 1)
                                break
                            elif emin_misin == 2:
                                break
                            else:
                                print("Yanlış tercih yaptınız")
                        else:
                            print("{} esyasini kullandin.".format(self.karakter.cantasi[islem - 1].get('isim')))
                            print("{} susuzluk giderildi.\nYeni susuzlugun {}.".format(
                                actor.susuzluk_giderme(self.karakter.cantasi[islem - 1].get("deger"),
                                                       self.karakter.susuzluk),
                                actor.susuzluk))
                            self.karakter.cantasi.pop(islem - 1)
                            break
                elif self.karakter.cantasi[islem - 1].get("tip") == 9:
                    self.karakter.cantasi.append(self.karakter.silahi.pop(0))
                    self.karakter.silahi.append(self.karakter.cantasi.pop(self.karakter.cantasi[islem - 1] - 1))
                elif self.karakter.cantasi[islem - 1].get("tip") == 10:
                    self.karakter.cantasi.append(self.karakter.silahi.pop(0))
                    self.karakter.silahi.append(self.karakter.cantasi.pop(self.karakter.cantasi[islem - 1] - 1))
                elif self.karakter.cantasi[islem - 1].get("tip") == 11:
                    ""
            else:
                print("Yanlış seçim yaptınız.")
                continue
