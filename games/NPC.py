from games.Actor import Actor


class Kotu(Actor):
    dusecekler = [{
        "isim": "Kılıç", "hasar": 5, 'fiyat': 2, "satis_fiyati": 1, "tip": 10},
        {'isim': 'Ekmek', 'satis_fiyati': 0.5, 'tip': 4, 'deger': 10},
        {'isim': 'Su', 'satis_fiyati': 0.5, 'tip': 3, 'deger': 40}]
    # dusecek_altin = 10
    silahi = [{'isim': 'Kılıç', 'hasar': 2}]


"""
Tip 1 : Can doldurur.
Tip 2 : Enerji doldurur.
Tip 3 : Susuzluk giderir.
Tip 4 : Açlık giderir.
Tip 9 : Balta türevi.
Tip 10 : Kılıç türevi.
Tip 11 : Kalkan türevi.
"""


class Satici(Actor):
    urunler = [
        {'isim': 'Ekmek', 'satis_fiyati': 0.5, 'fiyat': 1, 'tip': 4, 'deger': 10},
        {'isim': 'Balık', 'satis_fiyati': 1, 'fiyat': 2, 'tip': 1, 'deger': 25},
        {'isim': 'Et', 'satis_fiyati': 1, 'fiyat': 2, 'tip': 4, 'deger': 50},
        {'isim': 'Su', 'satis_fiyati': 0.5, 'fiyat': 1, 'tip': 3, 'deger': 40},
        {'isim': 'Kahve', 'satis_fiyati': 1, 'fiyat': 2, 'tip': 2, 'deger': 50},
        {'isim': 'Bitki Çayı', 'satis_fiyati': 0.5, 'fiyat': 1, 'tip': 1, 'deger': 10},
    ]

    def etkilesim(self, secenek=0):
        if secenek == 0:
            urunlistesi = []
            urunid = 0
            print('Merhaba, Ben {}\n size nasıl hizmet edebilirim ?'.format(self.isim))
            for urun in self.urunler:
                urunid += 1
                print("{} - {} ({} Altın)".format(
                    urunid, urun.get('isim'), urun.get('fiyat')
                ))
            return None
        else:
            if len(self.urunler) >= secenek:
                return self.urunler[secenek - 1]


class Demirci(Satici):
    urunler = [
        {'isim': 'Balta', 'fiyat': 4, 'tip': 9, 'hasar': 10},
        {'isim': 'Kılıç', 'fiyat': 2, 'tip': 10, 'hasar': 5},
        {'isim': 'Kalkan', 'fiyat': 2, 'tip': 11, 'koruma': 30},
    ]
