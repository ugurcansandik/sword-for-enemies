from random import randint

from games.NPC import Demirci, Satici, Kotu


class Level:
    oyuncular = []
    yancilar_npc = []
    kotuler = []
    gun = 0
    max_gun = 10
    """
    yon = 0 yukarı
    yon = 1 sol
    yon = 2 sag
    yon = 3 asagi
    """
    ana_karakter_kare = [0, 0]
    harita_boyutu = [40, 40]
    merkez = [0, 0]

    def __init__(self, oyuncu):

        for i in range(10):
            x = randint(-20, 20)
            y = randint(-20, 20)
            kotu = Kotu()
            kotu.altin = randint(0, 10)
            kotu.deneyim_seviyesi = randint(1, 3)
            self.kotuler.append({'x': x, 'y': y, 'kim': kotu}, )

        demirci = Demirci()
        demirci.isim = 'Haydar'

        satici = Satici()
        satici.isim = 'Erşan Kuneri'

        self.yancilar_npc = [
            {'x': 1, 'y': 0, 'kim': demirci},
            {'x': 0, 'y': 1, 'kim': satici}
        ]

        self.oyuncular.append(oyuncu)

    def kotuleri_ara(self):
        id = 0
        for kotu in self.kotuler:
            id += 1
            if ((self.ana_karakter_kare[0] - 1 <= kotu.get('x') <= self.ana_karakter_kare[0] + 1) or (
                    self.ana_karakter_kare[1] - 1 <= kotu.get('y') <= self.ana_karakter_kare[1] + 1)):
                if kotu.get('kim').yasiyormu:
                    print('{} - {} level kotu'.format(id, kotu.get('kim').deneyim_seviyesi))

    def etrafa_bak(self):
        id = 0
        for yanci in self.yancilar_npc:
            id += 1
            if (self.ana_karakter_kare[0] - 1 <= yanci.get('x') <= self.ana_karakter_kare[0] + 1) or (
                    self.ana_karakter_kare[1] - 1 <= yanci.get('y') <= self.ana_karakter_kare[1] + 1):
                print('{} - {}'.format(id, yanci.get('kim').isim))

    def ilerle(self, kackare=1, yon=0):
        if yon == 0:
            self.ana_karakter_kare[1] = self.ana_karakter_kare[1] + kackare
        elif yon == 3:
            self.ana_karakter_kare[1] = self.ana_karakter_kare[1] - kackare

        elif yon == 1:
            self.ana_karakter_kare[0] = self.ana_karakter_kare[0] - kackare
        elif yon == 2:
            self.ana_karakter_kare[0] = self.ana_karakter_kare[0] + kackare
        self.oyuncular[0].susuzluk -= 3
        if self.oyuncular[0].susuzluk < 0:
            self.oyuncular[0].can -= 4
            if self.oyuncular[0].can <= 0:
                self.oyuncular[0].yasiyormu = False
                print("Öldün")
                exit()
            self.oyuncular[0].susuzluk = 0
            print(
                "Kalan susuzluk 0. Susuzlugunu gidermegigin surece canın azalacak.\n"
                "Kalan canin = {}.".format(self.oyuncular[0].can))
        else:
            print("Kalan susuzluk {}.".format(self.oyuncular[0].susuzluk))
        self.oyuncular[0].aclik -= 3
        if self.oyuncular[0].aclik < 0:
            self.oyuncular[0] -= 4
            if self.oyuncular[0] <= 0:
                self.oyuncular[0].yasiyormu = False
                print("Öldün")
                exit()
            self.oyuncular[0].aclik = 0
            print("Kalan aclik = 0. Acligini gidermedigin surece canın azalacak.\n"
                  "Kalan canin = {}".format(self.oyuncular[0].can))
        else:
            print("Kalan aclik {}.".format(self.oyuncular[0].aclik))

    def vurma(self, kim, kime):
        ""

    def etkilesim(self, kim, secim=0):
        yanci = self.yancilar_npc[kim - 1]
        urun = yanci.get('kim').etkilesim(secim)
        if urun is not None:
            return urun
