class Actor:
    can = 100
    enerji = 100
    susuzluk = 100
    aclik = 100
    guc = 1
    zeka = 1
    ceviklik = 1
    kullanilabilir_yetenek_puani = 0
    deneyim = 0
    deneyim_seviyesi = 1
    yasiyormu = True
    cantasi = [{'isim': 'Et', 'satis_fiyati': 1, 'fiyat': 2, 'tip': 4, 'deger': 50},
               {'isim': 'Su', 'satis_fiyati': 0.5, 'tip': 3, 'deger': 40}]
    giydikleri = []
    silahi = [{"isim": "Kılıç", "hasar": 5, 'fiyat': 2, "satis_fiyati": 1, "tip": 10}]
    altin = 3
    isim = ""

    def iyilestirme(self, kaccan, can):
        if can + kaccan >= 100:
            doldurulan_can = 100 - kaccan
            can = 100
            return doldurulan_can
        else:
            doldurulan_can=kaccan
            can += kaccan
            return doldurulan_can

    def enerji_doldur(self, kacenerji, enerji):
        if enerji + kacenerji >= 100:
            doldurulan_enerji = 100 - enerji
            enerji = 100
            return doldurulan_enerji
        else:
            doldurulan_enerji = kacenerji
            enerji += kacenerji
            return doldurulan_enerji

    def hasar(self, kaccan):
        if self.can - kaccan > 0:
            self.can -= kaccan
        else:
            self.yasiyormu = False

    def aclik_giderme(self, kac_aclik, aclik):
        if aclik + kac_aclik >= 100:
            giderilen_aclik = 100 - aclik
            aclik = 100
            return giderilen_aclik
        else:
            giderilen_aclik = kac_aclik
            aclik += kac_aclik
            return giderilen_aclik

    def susuzluk_giderme(self, kac_susuzluk, susuzluk):
        if susuzluk + kac_susuzluk >= 100:
            giderilen_susuzluk = 100 - susuzluk
            susuzluk = 100
            return giderilen_susuzluk
        else:
            giderilen_susuzluk = kac_susuzluk
            susuzluk += kac_susuzluk
            return giderilen_susuzluk
